class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(user: :company).order(created_at: :desc).to_a.first(20)
  end
end
