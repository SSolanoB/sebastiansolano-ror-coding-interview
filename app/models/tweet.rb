# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }

  validate :uniqueness_per_day

  private

  def uniqueness_per_day
    condition = user.tweets.where(body: body).where('created_at >= (?)', 24.hours.ago).present?
    errors.add(:base, 'There is some tweet created before') if condition
  end
end
